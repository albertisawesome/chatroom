"use strict";

Router.configure({
  layoutTemplate: "ApplicationLayout"
});

Router.route("/", function () {
  this.render("home");
}, {
  name: "home"
});